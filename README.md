# Django-basic-api


## Tạo PostgreSQL theo file query.sql
## Thiết lập môi trường 
```
python -m venv env
env\Scripts\activate
python -m pip install Django
pip install djangorestframework
```

## Vào project 
```
cd app_test
```

## Cài đặt PostgreSQL
```
pip install psycopg2-binary
```

## Cài đặt middleware 'django-cors-headers':
```
pip install django-cors-headers
```

## Thiết lập app connect databas
## Tạo data theo file query.sql trên
## Vào file setting.py thiết lập cho phù hợp với db cần connect theo cấu trúc:
```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'test',
        'USER': 'postgres',
        'PASSWORD': '123',
        'HOST': 'localhost',
        'PORT': '5432'
    }
}
```

## Khời tạo và run app
```
py manage.py migrate
py manage.py runserver
```

## Trường hợp bị lỗi "django.db.utils.ProgrammingError: relation "company" already exists" thực thi câu lệnh sau rồi run lại app:
```
python manage.py migrate --fake
```

## Đường dẫn khởi chạy api
```
http://127.0.0.1:8000/company/
```





