import axios from "axios";
import React, { useEffect, useState } from "react";
import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Container from "react-bootstrap/Container";

const initPerson = {
  id: "",
  name: "",
  age: "",
  address: "",
  salary: "",
  join_date: "",
};

function App() {
  const [company, setCompany] = useState([]);
  const [search, setSeacrh] = useState();
  const [person, setPerson] = useState(initPerson);
  const [editCheck, setEditCheck] = useState(false);

  useEffect(() => {
    handleGetData();
  }, []);

  const handleGetData = () => {
    axios
      .get(`http://localhost:8000/company/`)
      .then((res) => {
        if (res.status === 200) {
          setCompany(res.data);
        }
      })
      .catch((error) => {
        alert(error);
      });
  };

  function hanldeDelete(id) {
    axios
      .delete(`http://localhost:8000/company/${id}/`)
      .then((res) => {
        if (res.status === 204) {
          setCompany(company.filter((company) => company.id !== id));
        }
      })
      .catch((error) => {
        alert(error);
      });
  }

  const handleSearch = () => {
    axios
      .get(`http://localhost:8000/company/search/?q=${search}`)
      .then((res) => {
        if (res.status === 200) {
          setCompany(res.data);
        }
      })
      .catch((error) => {
        alert(error);
      });
  };

  const handleSubmit = () => {
    if (editCheck) {
      axios
        .put(`http://localhost:8000/company/${person.id}/`, person)
        .then((res) => {
          if (res.status === 200) {
            handleGetData();
            alert("Update success")
          }
        })
        .catch((error) => {
          alert(error);
        });
    } else {
      axios
        .post(`http://localhost:8000/company/`, person)
        .then((res) => {
          if (res.status === 201) {
            handleGetData();
            setPerson(initPerson)
            alert("Create success")
          }
        })
        .catch((error) => {
          alert(error);
        });
    }
  };

  return (
    <div className="App">
      <Container>
        <div className="float-right">
          <Form className="d-flex" style={{ width: 500, float: "right" }}>
            <Form.Control
              type="search"
              placeholder="Search"
              className="me-2"
              aria-label="Search"
              onChange={(e) => setSeacrh(e.target.value)}
            />
            <Button variant="outline-success" onClick={handleSearch}>
              Search
            </Button>
          </Form>
        </div>

        <Table striped bordered hover>
          <thead>
            <tr>
              <th>id</th>
              <th>name</th>
              <th>age</th>
              <th>address</th>
              <th>salary</th>
              <th>join_date</th>
              <th>edit</th>
              <th>detele</th>
            </tr>
          </thead>
          <tbody>
            {company.map((item) => (
              <tr key={item.id}>
                <td>{item.id}</td>
                <td>{item.name}</td>
                <td>{item.age}</td>
                <td>{item.address}</td>
                <td>{item.salary}</td>
                <td>{item.join_date}</td>
                <td>
                  <button
                    onClick={() => {
                      setEditCheck(true);
                      setPerson(item);
                    }}
                  >
                    &#x270E;
                  </button>{" "}
                </td>
                <td>
                  <button onClick={() => hanldeDelete(item.id)}>&times;</button>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
        {editCheck ? <h1>Update</h1> : <h1>Create</h1>}
        <Form className="col-md-6">
          {editCheck && <Form.Label className="col-md-3">id</Form.Label>}
          {editCheck && (
            <Form.Control
              className="col-md-3"
              type="text"
              placeholder="id"
              onChange={(e) => setPerson({ ...person, name: e.target.value })}
              value={person.id}
              disabled
            />
          )}

          <Form.Label className="col-md-3">name</Form.Label>
          <Form.Control
            className="col-md-3"
            type="text"
            placeholder="name"
            onChange={(e) => setPerson({ ...person, name: e.target.value })}
            value={person.name}
          />
          <Form.Label>age</Form.Label>
          <Form.Control
            type="number"
            placeholder="age"
            onChange={(e) => setPerson({ ...person, age: e.target.value })}
            value={person.age}
          />
          <Form.Label>address</Form.Label>
          <Form.Control
            type="text"
            placeholder="address"
            onChange={(e) => setPerson({ ...person, address: e.target.value })}
            value={person.address}
          />
          <Form.Label>salary</Form.Label>
          <Form.Control
            type="number"
            placeholder="salary"
            onChange={(e) => setPerson({ ...person, salary: e.target.value })}
            value={person.salary}
          />
          <Form.Label>join_date</Form.Label>
          <Form.Control
            type="date"
            placeholder="join_date"
            onChange={(e) =>
              setPerson({ ...person, join_date: e.target.value })
            }
            value={person.join_date}
          />
          <br />
          <div className="d-flex">
            <Button variant="outline-success" onClick={handleSubmit}>
              Submit
            </Button>
            <Button
              variant="outline-success"
              type="reset"
              onClick={() => {
                setPerson(initPerson);
                setEditCheck(false)
              }}
            >
              Reset
            </Button>
          </div>
        </Form>
      </Container>
    </div>
  );
}

export default App;
