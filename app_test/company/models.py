from django.db import models

# Create your models here.


class Company(models.Model):
    id = models.AutoField(primary_key=True, db_column="id")
    name = models.TextField(db_column="name")
    age = models.IntegerField(db_column="age")
    address = models.CharField(db_column="address")
    salary = models.IntegerField(db_column="salary")
    join_date = models.DateField(db_column="join_date")

    class Meta:
        db_table = 'company'
