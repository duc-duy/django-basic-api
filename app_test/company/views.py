from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import Company
from .serializers import CompanySerializer
# Create your views here.


class MyModelList(APIView):
    serializer_class = CompanySerializer

    def get(self, request, format=None):
        mymodels = Company.objects.all()
        serializer = CompanySerializer(mymodels, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = CompanySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class MyModelSearchView(APIView):
    serializer_class = CompanySerializer
    
    def get(self, request):
        query = request.query_params.get('q', '')
        if query == '':
            data = Company.objects.all()
        else:
            data = Company.objects.filter(name__icontains=query) | Company.objects.filter(address__icontains=query) | Company.objects.filter(age__icontains=query)
        serializer = CompanySerializer(data, many=True)
        return Response(serializer.data)


class MyModelDetail(APIView):
    serializer_class = CompanySerializer

    def get_object(self, pk):
        return get_object_or_404(Company, pk=pk)

    def get(self, request, pk, format=None):
        mymodel = self.get_object(pk)
        serializer = CompanySerializer(mymodel)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        mymodel = self.get_object(pk)
        serializer = CompanySerializer(mymodel, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        mymodel = self.get_object(pk)
        mymodel.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
